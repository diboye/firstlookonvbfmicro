//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Aug 31 16:22:08 2021 by ROOT version 6.22/09
// from TTree VBFH125Nominal/VBFH125Nominal
// found on file: VBFH125.root
//////////////////////////////////////////////////////////

#ifndef plotting_h
#define plotting_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>
#include <TH1F.h>
#include <TH2F.h>
#include <THStack.h>

// Headers needed by this particular selector
#include <vector>
#include <TLorentzVector.h>



class plotting : public TSelector {
public :
   TTreeReader     fReader;  //!the tree reader
   TTree          *fChain = 0;   //!pointer to the analyzed TTree or TChain

   TH2D* hist_j3EtaPt;

  TH1D*hist_mjj;
  TH1D*hist_jj_pT1;
  TH1D*hist_jj_pT2;
  TH1D*hist_jj_pT3;
  int event;

  TH1D*hist_el_pT1;
  TH1D*hist_el_phi;
  
  TH1D*hist_pTV;
  TH1D*hist_MV;
  TH1D*hist_dphi_MET_Llep;
  TH1D*hist_dphi_METnoLep_Llep;
  TH2D*hist_pTV_vs_MET_nolep;
  TH1D* hist_dphi_MET_PTV;
  TH1D*hist_dphi_METnoLep_PTV;
  TH1D*hist_MET_phi;
  TH1D*hist_METnoLep_phi;
  TH1D*hist_V_phi;
    
  TH1D*hist_el_pT2;

  TH1D*hist_jj_dphi;
  TH1D*hist_jj_deta;

  TH1D*hist_met_soft_tst_et;
  TH1D*hist_met_tst_et;
  TH1D*hist_n_jets;
  TH1D*hist_n_el;
  TH1D*hist_n_mu;

  TH1D*hist_n_truth_el;
  TH1D*hist_n_truth_mu;

  TH1D*hist_ldJet_eta;
  TH1D*hist_sldJet_eta;
  TH1D*hist_thirdJet_eta;
  TH1D*hist_met_tst_nolep_et;

  TH1D*hist_ldJet_phi;
  TH1D*hist_sldJet_phi;
  TH1D*hist_jet_notIsHS;
  TH1D*hist_jet_isHS;
  TH1D*hist_jet_isHS_or_not;
  

  TH1D*hist_j3Cent;
  TH1D*hist_mj34;
  TH1D*hist_j3_dRj1;
  TH1D*hist_j3_dRj2;
  TH1D*hist_j3_minDR;

  TH1D*hist_jet_eta_size;
  TH1D*hist_jet_phi_size;
  
  std::ofstream outfile;

  TH1D*hist_met_significance;
  TH1D* hist_maxCentrality;

  TH1D*hist_j3_min_mj_over_mjj;
  
  TFile* fOut;
  vector<bool> jet_isHS_0;
  vector<bool> jet_isHS_1;

  TLorentzVector v1_lep,v2_lep,v12_lep;
  
  bool Z_ee = false;
  bool Z_mumu = false;
  bool Z_tautau = false;
  double mu_mass = 105.7; //MeV
  double el_mass = 0.5; //MeV

   // Readers to access the data (delete the ones you do not need).
   TTreeReaderValue<Float_t> w = {fReader, "w"};
  TTreeReaderValue<Float_t> pdfw = {fReader, "pdfw"};
  TTreeReaderValue<UInt_t> runNumber = {fReader, "runNumber"};
   TTreeReaderValue<ULong64_t> eventNumber = {fReader, "eventNumber"};
   TTreeReaderValue<Int_t> trigger_met = {fReader, "trigger_met"};
   TTreeReaderValue<Int_t> trigger_met_encoded = {fReader, "trigger_met_encoded"};
   TTreeReaderValue<Int_t> trigger_lep = {fReader, "trigger_lep"};
   TTreeReaderValue<Float_t> jj_mass = {fReader, "jj_mass"};
   TTreeReaderValue<Float_t> jj_deta = {fReader, "jj_deta"};
   TTreeReaderValue<Float_t> jj_dphi = {fReader, "jj_dphi"};
   TTreeReaderValue<Float_t> met_tst_et = {fReader, "met_tst_et"};
   TTreeReaderValue<Float_t> met_tst_nolep_et = {fReader, "met_tst_nolep_et"};
   TTreeReaderValue<Float_t> met_phi = {fReader, "met_phi"};
   TTreeReaderValue<Float_t> met_tst_nolep_phi = {fReader, "met_tst_nolep_phi"};
   TTreeReaderValue<Float_t> met_soft_tst_et = {fReader, "met_soft_tst_et"};
   TTreeReaderValue<Float_t> met_cst_jet = {fReader, "met_cst_jet"};
   TTreeReaderArray<float> mu_charge = {fReader, "mu_charge"};
   TTreeReaderArray<float> mu_pt = {fReader, "mu_pt"};
   TTreeReaderArray<float> el_charge = {fReader, "el_charge"};
   TTreeReaderArray<float> el_pt = {fReader, "el_pt"};
   TTreeReaderArray<float> mu_phi = {fReader, "mu_phi"};
   TTreeReaderArray<float> el_phi = {fReader, "el_phi"};
   TTreeReaderArray<float> mu_eta = {fReader, "mu_eta"};
   TTreeReaderArray<float> el_eta = {fReader, "el_eta"};
  TTreeReaderArray<float> jet_pt = {fReader, "jet_pt"};
   TTreeReaderArray<float> jet_eta = {fReader, "jet_eta"};
   TTreeReaderArray<float> jet_phi = {fReader, "jet_phi"};
   TTreeReaderArray<float> jet_m = {fReader, "jet_m"};

  TTreeReaderArray<float> boson_pt = {fReader, "boson_pt"};
   TTreeReaderArray<float> boson_phi = {fReader, "boson_phi"};
  TTreeReaderArray<float> boson_m = {fReader, "boson_m"};
  TTreeReaderArray<float> boson_eta = {fReader, "boson_eta"};
  TTreeReaderArray<int> boson_status = {fReader, "boson_status"};
  
   TTreeReaderValue<Float_t> met_significance = {fReader, "met_significance"};
   TTreeReaderValue<Float_t> max_mj_over_mjj = {fReader, "max_mj_over_mjj"};
   TTreeReaderValue<Float_t> maxCentrality = {fReader, "maxCentrality"};
   TTreeReaderValue<Float_t> mj34 = {fReader, "mj34"};
   TTreeReaderArray<float> j3_centrality = {fReader, "j3_centrality"};
   TTreeReaderArray<float> j3_dRj1 = {fReader, "j3_dRj1"};
   TTreeReaderArray<float> j3_dRj2 = {fReader, "j3_dRj2"};
   TTreeReaderArray<float> j3_minDR = {fReader, "j3_minDR"};
   TTreeReaderArray<float> j3_mjclosest = {fReader, "j3_mjclosest"};
   TTreeReaderArray<float> j3_min_mj = {fReader, "j3_min_mj"};
   TTreeReaderArray<float> j3_min_mj_over_mjj = {fReader, "j3_min_mj_over_mjj"};
   TTreeReaderValue<Int_t> n_jet = {fReader, "n_jet"};
   TTreeReaderValue<Int_t> n_el = {fReader, "n_el"};
   TTreeReaderValue<Int_t> n_baseel = {fReader, "n_baseel"};
   TTreeReaderValue<Int_t> n_mu = {fReader, "n_mu"};
   TTreeReaderValue<Int_t> n_basemu = {fReader, "n_basemu"};
   TTreeReaderArray<Bool_t> jet_isHS = {fReader, "jet_isHS"};
   TTreeReaderValue<Int_t> n_truth_el = {fReader, "n_truth_el"};
  TTreeReaderValue<Int_t> n_truth_jet = {fReader, "n_truth_jet"};
   TTreeReaderValue<Int_t> n_truth_mu = {fReader, "n_truth_mu"};
  TTreeReaderArray<float> truth_el_pt = {fReader, "truth_el_pt"};
  TTreeReaderArray<float> truth_el_eta = {fReader, "truth_el_eta"};
  TTreeReaderArray<float> truth_el_phi = {fReader, "truth_el_phi"};

  TTreeReaderArray<float> truth_mu_pt = {fReader, "truth_mu_pt"};
  TTreeReaderArray<float> truth_mu_eta = {fReader, "truth_mu_eta"};
  TTreeReaderArray<float> truth_mu_phi = {fReader, "truth_mu_phi"};

  TTreeReaderArray<float> truth_jet_pt = {fReader, "truth_jet_pt"};
  TTreeReaderArray<float> truth_jet_eta = {fReader, "truth_jet_eta"};
  TTreeReaderArray<float> truth_jet_phi = {fReader, "truth_jet_phi"};


   plotting(TTree * /*tree*/ =0) { }
   virtual ~plotting() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   ClassDef(plotting,0);

};

#endif

#ifdef plotting_cxx
void plotting::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the reader is initialized.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   fReader.SetTree(tree);
}

Bool_t plotting::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}


#endif // #ifdef plotting_cxx
