#define plotting_cxx
#include "plotting.h"
#include <TH2.h>
#include <TStyle.h>
#include <TH1F.h>
#include <TAxis.h>
#include <TCanvas.h>
#include <TLine.h>
#include <THStack.h>
#include <TString.h>
#include <TStyle.h>
#include "TLegend.h"
#include "TLatex.h"
#include "TPaveText.h"
#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include <iomanip>
#include <THStack.h>
void plotting::Begin(TTree * /*tree*/)
{
  TString option = GetOption();
  TString file = GetOption();

   if (file.IsNull())
     file="summary.root";
   else {
     Ssiz_t pos = file.Last('.');
     if (pos<0) pos=file.Length();
     file.Replace(pos,20,".root");
   }
   
   fOut = TFile::Open(file, "RECREATE");

    if (!file.SubString("Z_ee").IsNull()) 
     { 
       Z_ee = true;
     }
   else if (!file.SubString("Z_mumu").IsNull())
   // else 
    {
       Z_mumu = true;
     }
   else if (!file.SubString("Z_tautau").IsNull())
     {
       Z_tautau = true;
     }
cout << "file = " << file << endl; 
     cout << "Z_ee = " << Z_ee << endl;
     cout << "Z_mumu = " << Z_mumu << endl;
     cout << "Z_tautau = " << Z_tautau << endl;
   
  
  hist_mjj =  new TH1D("hist_mjj" ,"truth_smearing; m_{jj} [GeV];entries / [100 GeV]",60,0,6000);
  hist_jj_pT1 =  new TH1D("hist_jj_pT1" ,"truth_smearing; j_p_{T1} [GeV];entries / [20 GeV]",50,0,1000);

  hist_pTV =  new TH1D("hist_pTV" ,"truth_smearing; p_{T}V [GeV];entries / [20 GeV]",50,0,1000);
  hist_MV =  new TH1D("hist_MV" ,"truth_smearing; MV [GeV];entries / [1 GeV]",150,0,150);

  hist_pTV_vs_MET_nolep =  new TH2D("hist_pTV_vs_MET_nolep", "truth_smearing; p_{T}V [GeV]; MET^{nolep} [GeV]", 50, 0, 1000, 50, 0, 1000.);
  hist_dphi_MET_Llep = new TH1D("hist_dphi_MET_Llep" ,"truth_smearing; #Delta #phi(MET, LeadEl);entries / [0.1]",200,-10,10);
  hist_dphi_METnoLep_Llep = new TH1D("hist_dphi_METnoLep_Llep" ,"truth_smearing; #Delta #phi(MET^{nolep}, LeadEl);entries / [0.1]",200,-10,10);
  hist_dphi_MET_PTV = new TH1D("hist_dphi_MET_PTV" ,"truth_smearing; #Delta #phi(MET,PTV);entries / [0.1]",200,-10,10);
  hist_dphi_METnoLep_PTV = new TH1D("hist_dphi_METnoLep_PTV" ,"truth_smearing; #Delta #phi(MET^{nolep},PTV);entries / [0.1]",200,-10,10);
  hist_MET_phi =  new TH1D("hist_MET_phi" ,"truth_smearing; MET #phi;entries / [0.1]",200,-10,10);
  hist_METnoLep_phi =  new TH1D("hist_METnoLep_phi" ,"truth_smearing; MET^{noLep} #phi;entries / [0.1]",200,-10,10);
  hist_V_phi =  new TH1D("hist_PTV_phi" ,"truth_smearing; V #phi;entries / [0.1]",200,-10,10);
  hist_el_phi =  new TH1D("hist_el_phi" ,"truth_smearing; Lel #phi;entries / [0.1]",200,-10,10);
    
  
  hist_jj_pT2 =  new TH1D("hist_jj_pT2" ,"truth_smearing; j_p_{T2} [GeV];entries / [20 GeV]",50,0,1000);
  hist_jj_pT3 =  new TH1D("hist_jj_pT3" ,"truth_smearing; j_p_{T3} [GeV];entries / [20 GeV]",50,0,1000);
  hist_jj_dphi =  new TH1D("hist_jj_dphi" ,"truth_smearing; #Delta#phi_{jj} ;entries / [0.1 GeV]",80,0,8);
  hist_jj_deta =  new TH1D("hist_jj_deta" ,"truth_smearing; #Delta#eta_{jj} ;entries / [0.1 GeV]",120,0,12);
  hist_met_soft_tst_et =  new TH1D("hist_met_soft_tst_et" ,"truth_smearing; E_{T}^{miss, soft} [GeV];entries / [2 GeV]",50,0,100);
  hist_met_tst_et =  new TH1D("hist_met_tst_et" ,"truth_smearing; E_{T}^{miss} [GeV];entries / [2 GeV]",50,0,1000);

  hist_met_significance =  new TH1D("hist_met_significance" ,"truth_smearing; E_{T}^{miss, sig} [GeV];entries / [2 GeV]",50,0,100);

  hist_met_tst_nolep_et =  new TH1D("hist_met_tst_nolep_et" ,"truth_smearing; E_{T}^{miss, noLep} [GeV];entries / [2 GeV]",50,0,1000);

  hist_n_jets =  new TH1D("hist_n_jets" ,"truth_smearing; Number of jets ;entries / [1 GeV]",10,0,10);
  hist_n_el =  new TH1D("hist_n_el" ,"truth_smearing; Number of el ;entries / [1 GeV]",10,0,10);
  hist_n_mu =  new TH1D("hist_n_mu" ,"truth_smearing; Number of mu ;entries / [1 GeV]",10,0,10);

  hist_n_truth_el =  new TH1D("hist_n_truth_el" ,"truth_smearing; Number of truth_el ;entries / [1 GeV]",10,0,10);
  hist_n_truth_mu =  new TH1D("hist_n_truth_mu" ,"truth_smearing; Number of truth_mu ;entries / [1 GeV]",10,0,10);

  hist_jet_eta_size =  new TH1D("hist_jet_eta_size" ,"truth_smearing; jet(#eta) size ;entries / [1 GeV]",10,0,10);
  hist_jet_phi_size =  new TH1D("hist_jet_phi_size" ,"truth_smearing; jet(#phi) size ;entries / [1 GeV]",10,0,10);
  

  hist_jet_isHS =  new TH1D("hist_jet_isHS" ,"truth_smearing; jet_isHS size ;entries / [1 GeV]",10,0,10);
  hist_jet_notIsHS =  new TH1D("hist_jet_notIsHS" ,"truth_smearing; jet_notIsHS size ;entries / [1 GeV]",10,0,10);

  hist_jet_isHS_or_not =  new TH1D("hist_jet_isHS_or_not" ,"truth_smearing; jet_isHS or not size ;entries / [1 GeV]",10,0,10);
  

  hist_ldJet_eta =  new TH1D("hist_ldJet_eta" ,"truth_smearing; |Leading jet #eta|;entries / [0.1]",200,-10,10);
  hist_sldJet_eta =  new TH1D("hist_sldJet_eta" ,"truth_smearing; Sub-Leading jet #eta;entries / [0.1]",200,-10,10);
  hist_thirdJet_eta =  new TH1D("hist_thirdJet_eta" ,"truth_smearing; Third jet #eta;entries / [0.1]",200,-10,10);

  hist_ldJet_phi =  new TH1D("hist_ldJet_phi" ,"truth_smearing; Leading jet #phi;entries / [0.1]",200,-10,10);
  hist_sldJet_phi =  new TH1D("hist_sldJet_phi" ,"truth_smearing; Sub-Leading jet #phi;entries / [0.1]",200,-10,10);

  hist_j3Cent =  new TH1D("hist_j3Cent" ,"truth_smearing; j3 centrality ;entries / [1 GeV]",10,0,1);

  hist_j3_min_mj_over_mjj =  new TH1D("hist_j3_min_mj_over_mjj" ,"truth_smearing; min m_{j1/2,j3}/m_{j1.j2} ;entries / [1 GeV]",10,0,1);

  hist_mj34 =  new TH1D("hist_mj34" ,"truth_smearing; mj34 [GeV];entries / [1 GeV]",10,50,1000);
  hist_maxCentrality =  new TH1D("hist_maxCentrality" ,"truth_smearing; maxCentrality;entries / [1 GeV]",10,0,2);

  hist_j3_dRj1 =  new TH1D("hist_j3_dRj1" ,"truth_smearing; j3_dRj1;entries / [1 GeV]",10,0,2);
  hist_j3_dRj2 =  new TH1D("hist_j3_dRj2" ,"truth_smearing; j3_dRj3;entries / [1 GeV]",10,0,2);
  hist_j3_minDR =  new TH1D("hist_j3_minDR" ,"truth_smearing; j3_minDR;entries / [1 GeV]",10,0,2);
  
  hist_j3EtaPt = new TH2D("hist_j3EtaPt", "truth_smearing; #eta(j3); p_T(j3)", 20, -5, 5, 50, 0, 300.);

  hist_el_pT1 =  new TH1D("hist_el_pT1" ,"all; el_p_{T1} [GeV];entries / [10 GeV]",50,0,500);
  hist_el_pT2 =  new TH1D("hist_el_pT2" ,"all; el_p_{T2} [GeV];entries / [10 GeV]",50,0,500);

   

  outfile.open("printing_Zee.txt"); // append instead of overwrite
  //outfile << "n_jets" <<  '\t' << "jet_pT_eta" <<  '\t' << "jet_pT_phi"  << '\t' << "jet_isHS_false"  << '\t' << "jet_isHS_true" << '\t' << "n_truth_el"  << '\t' << "n_el" << '\t' << "n_truth_mu" << '\t' << "n_mu"<< endl;
     event=1;
  
}

void plotting::SlaveBegin(TTree * /*tree*/)
{
   TString option = GetOption();

}

Bool_t plotting::Process(Long64_t entry)
{
  fReader.SetLocalEntry(entry);
  //fReader.SetLocalEntry(10000);
  //cout << "test" << endl;
  
   double mjj = *jj_mass*0.001;
   double m_j34 = *mj34*0.001;
   double met_soft = *met_soft_tst_et*0.001;
   double met = *met_tst_et*0.001;
   double met_nolep = *met_tst_nolep_et*0.001;
   double met_sig = *met_significance*0.001;
   double JetpT1 = jet_pt[0]*0.001;
   double JetpT2 = jet_pt[1]*0.001;
   double JetpT3 = jet_pt[2]*0.001;
   double JetpT4 = jet_pt[4]*0.001;
   double met_after_cut;
   double metNoLep_after_cut;

   //if(met_nolep < 130) return kTRUE;
   if(JetpT1 < 60) return kTRUE;
   if(JetpT2 < 50) return kTRUE;
   if(mjj< 200) return kTRUE;
   if(*jj_deta < 3.5) return kTRUE;
   if(met_soft > 35) return kTRUE;
   if(fabs(jet_eta[0]) > 3.8 && JetpT1 > 50) return kTRUE;
   if(fabs(jet_eta[1]) > 3.8 && JetpT2 > 50) return kTRUE;
   //if(fabs(jet_eta[2]) > 3.8 && JetpT3 > 50) return kTRUE;
   //if(fabs(jet_eta[3]) > 3.8 && JetpT4 > 50) return kTRUE;
   if(!(jet_eta[0]*jet_eta[1] < 0)) return kTRUE;
    

   

     
   /*for(int njet =0; njet<*n_jet; njet++)
      {
	if(fabs(jet_eta[njet]) > 3.8) return kTRUE;
      }*/

   //if(*eventNumber!=818060)return kTRUE;
   
   //if(!(mjj > 200 && met_soft_noLep > 130)) return kTRUE;
   
   //if(*runNumber < 364100 || *runNumber > 364113) return kTRUE; //for Znunu DSIDS only
   //if (!(*runNumber >= 364114 && *runNumber <= 364127)) return kTRUE; // //Zee DSIDs only
   //if(*runNumber < 364128 || *runNumber > 364141) return kTRUE; //for Ztautau DSIDS only

   //if(*runNumber!=363234)
   /*if(boson_status[1]!=11)
     cout << boson_status[1] << endl;*/

   double elpT1=0.0;
   double elpT2=0.0;


    
   
    
   
   
   /*v1_el.SetPtEtaPhiM(el_pt[0],el_eta[0],el_phi[0],0.5);
   
   v2_el.SetPtEtaPhiM(el_pt[1],el_eta[1],el_phi[1],0.5);
   
   v12_el = v1_el + v2_el;

   v1_mu.SetPtEtaPhiM(mu_pt[0],mu_eta[0],mu_phi[0],105.7);
   
   v2_mu.SetPtEtaPhiM(mu_pt[1],mu_eta[1],mu_phi[1],105.7);
   
   v12_mu = v1_mu + v2_mu;*/
   
   int index_boson;
   TLorentzVector vSum_jet;
   TLorentzVector vSum_lep;
   

   //for electrons
   if(Z_ee)
     {
       if(*n_el!=2)return kTRUE;
       {
	 v1_lep.SetPtEtaPhiM(el_pt[0],el_eta[0],el_phi[0],mu_mass);
	 for(int nel =0; nel<*n_el; nel++)
	   {
	     TLorentzVector v_leps;
	     v_leps.SetPtEtaPhiM(el_pt[nel],el_eta[nel],el_phi[nel],mu_mass);
	       vSum_lep+= v_leps;
	     }
	 }
     }
   //cout << "test " << *n_el << endl;
    //for muons
   else if(Z_mumu)
     {
       if(*n_mu!=2)return kTRUE;
	 {
	   v1_lep.SetPtEtaPhiM(mu_pt[0],mu_eta[0],mu_phi[0],mu_mass);
	   for(int nmu =0; nmu<*n_mu; nmu++)
	     {
	       TLorentzVector v_leps;
	       v_leps.SetPtEtaPhiM(mu_pt[nmu],mu_eta[nmu],mu_phi[nmu],mu_mass);
	       vSum_lep+= v_leps;
	     }
	 }
     }
    //else return kTRUE;
   //for jets
   for(int njet =0; njet<*n_jet; njet++)
     {
       if(fabs(jet_eta[njet]) > 3.8 && jet_pt[njet]*0.001 < 50) continue;
       TLorentzVector v_jets;
       v_jets.SetPtEtaPhiM(jet_pt[njet],jet_eta[njet],jet_phi[njet],0);
       vSum_jet+= v_jets;
     }
   
   TLorentzVector vlepJet, vlepJet_nolep;
   vlepJet = - (vSum_lep+vSum_jet);
   vlepJet_nolep = - (vSum_jet);
   
   met_after_cut = vlepJet.Pt()*0.001;
   metNoLep_after_cut = vlepJet_nolep.Pt()*0.001;
   if(metNoLep_after_cut < 130) return kTRUE;
   
   
   hist_j3EtaPt->Fill(jet_eta[2], JetpT3);
   
   hist_jj_pT1->Fill(JetpT1);
   hist_jj_pT2->Fill(JetpT2);
   hist_jj_pT3->Fill(JetpT3);

    TLorentzVector v_met, v_met_nolep, v_boson;
   
     for(int i = 0; i < boson_m.GetSize(); i++)
      {
       	if(boson_m[i] > 0)
	  {
	    index_boson =i;
	    break;
	  }

      }
  
    v_boson.SetPtEtaPhiM(boson_pt[index_boson],boson_eta[index_boson],boson_phi[index_boson],boson_m[index_boson]);
    

    hist_el_pT1->Fill(v1_lep.Pt());
    hist_el_pT2->Fill(v1_lep.Pt());

   if(boson_pt.GetSize()!=0)
     {
       hist_pTV->Fill(boson_pt[0]*0.001);
       hist_pTV_vs_MET_nolep->Fill(boson_pt[index_boson]*0.001,metNoLep_after_cut);
       
     }
   
    if(boson_m.GetSize()!=0)
     {
       hist_MV->Fill(boson_m[0]*0.001);
     }
    
    //if(el_phi.GetSize()!=0)
    //{
       hist_dphi_MET_Llep->Fill(vlepJet.DeltaPhi(v1_lep));
       hist_dphi_METnoLep_Llep->Fill(vlepJet_nolep.DeltaPhi(v1_lep));
       hist_el_phi->Fill(v1_lep.Phi());
       //}
   

    
if(boson_phi.GetSize()!=0)
  {
    /*hist_dphi_MET_PTV->Fill(vlepJet.DeltaPhi(v_boson));
    hist_dphi_METnoLep_PTV->Fill(vlepJet_nolep.DeltaPhi(v_boson));*/
    hist_dphi_MET_PTV->Fill(vlepJet.DeltaPhi(vSum_lep));
    hist_dphi_METnoLep_PTV->Fill(vlepJet_nolep.DeltaPhi(vSum_lep));
    hist_V_phi->Fill(boson_phi[0]);
  }

 hist_MET_phi->Fill(*met_phi);
 hist_METnoLep_phi->Fill(*met_tst_nolep_phi);
 
 
    //cout << "test " << TLorentzVector::DeltaPhi(*met_phi, *met_tst_nolep_phi);  << endl;

 
   hist_mjj->Fill(mjj);
   hist_mj34->Fill(m_j34);
   hist_jj_deta->Fill(fabs(*jj_deta));
   hist_jj_dphi->Fill(fabs(*jj_dphi));
   hist_met_soft_tst_et->Fill(met_soft);
   hist_met_tst_nolep_et->Fill(met_nolep);
   //hist_met_tst_et->Fill(met);
   hist_met_tst_et->Fill(met_after_cut);
   hist_met_significance->Fill(met_sig);
   hist_n_jets->Fill(*n_jet);
   hist_n_el->Fill(*n_el);
   hist_n_mu->Fill(*n_mu);
   hist_ldJet_eta->Fill(jet_eta[0]);
   hist_sldJet_eta->Fill(jet_eta[1]);

   hist_ldJet_phi->Fill(jet_phi[0]);
   hist_sldJet_phi->Fill(jet_phi[1]);
   hist_thirdJet_eta->Fill(jet_eta[2]);

   hist_maxCentrality->Fill(*maxCentrality);

   if (jet_pt.GetSize() > 2){
   hist_j3Cent->Fill(j3_centrality[0]);
   hist_j3_min_mj_over_mjj->Fill(j3_min_mj_over_mjj[0]);
   
   }

   for (int i = 0; i < jet_isHS.GetSize(); i++)
     {
       if(jet_isHS[i]) jet_isHS_1.push_back(jet_isHS[i]);
       else if(!jet_isHS[i]) jet_isHS_0.push_back(jet_isHS[i]);
				     //cout << jet_isHS[0] << endl;
     }
   
  

     /*cout << v1_el.Pt()*0.001 << " test1 " << el_pt[0]*0.001<< endl;
   cout << v2_el.Pt()*0.001 << " test2 " << el_pt[1]*0.001<< endl;
   cout << v12_el.Pt()*0.001 << " test3 " << el_pt[0]*0.001+el_pt[1]*0.001<< endl;*/

    
    //met_after_cut = -sum_pt_jets;
   
   //cout << jet_isHS_0[0] << endl;
   //cout << "n jets " << *n_jet << " jet_pT_eta " << jet_eta.GetSize() << " jet_pT_eta " << jet_phi.GetSize() << " jet_isHS false " << jet_isHS_0.size() << " jet_isHS true " << jet_isHS_1.size() << " n truth el " << *n_truth_el << " n truth el "<< *n_truth_mu<<endl;
 
   //outfile <<  *n_jet  << '\t' << '\t' << jet_eta.GetSize()  << '\t' << '\t'<< jet_phi.GetSize()  << '\t' <<'\t' << jet_isHS_0.size()  << '\t' <<'\t' << jet_isHS_1.size()  << '\t' <<'\t' << *n_truth_el <<  '\t' << *n_el << '\t' <<'\t' << *n_truth_mu << '\t' << *n_mu << endl;

    if(*n_jet!=jet_eta.GetSize() || *n_jet!=jet_phi.GetSize() || jet_eta.GetSize()!=jet_phi.GetSize()) cout << " not expected " << endl;
    //if(*n_jet > 10) cout << *n_jet << endl;

    hist_n_truth_el->Fill(*n_truth_el);
    hist_n_truth_mu->Fill(*n_truth_mu);
    hist_jet_eta_size->Fill(jet_eta.GetSize());
    hist_jet_phi_size->Fill(jet_phi.GetSize());
    for (int i = 0; i < jet_isHS.GetSize(); i++)
     {
       hist_jet_isHS_or_not->Fill(jet_isHS[i]);
       if(jet_isHS[i]) hist_jet_isHS->Fill(jet_isHS[i]);
       else if(!jet_isHS[i]) hist_jet_notIsHS->Fill(jet_isHS[i]);
     }

    
   
    
    jet_isHS_1.clear();
    jet_isHS_0.clear();
    
    
    //if (event==500) return kTRUE;;

    /* for(int ntruthel =0; ntruthel<*n_truth_el; ntruthel++)
      {
	if(fabs(truth_el_eta[ntruthel] < 2.5))
	{
	  return kTRUE;
	}
      }*/
  
    //cout << " debug " <<endl;

     outfile << "***********  event number  *************" << *eventNumber << endl;

     //jets
     outfile << "============= Jets ==============" << endl;
     
     for(int njet =0; njet<*n_jet; njet++)
      {
	//if(fabs(jet_eta[njet] > 2.5))
	// {
	outfile << Form("reco_jet_pT_%d ",njet) << jet_pt[njet]*0.001 << '\t' << Form("reco_jet_eta_%d ",njet) <<  jet_eta[njet] << '\t' << Form("reco_jet_phi_%d ",njet) <<  jet_phi[njet] << '\t' << "reco_jetisHS " << jet_isHS[njet] << endl;
	// }

      }

	 outfile <<"reco_n_jets " << *n_jet  <<  endl;

     for(int ntruthjet =0; ntruthjet<*n_truth_jet; ntruthjet++)
      {
	//if(fabs(truth_jet_eta[ntruthjet] > 2.5))
	// {
		    outfile << Form("truth_jet_pT_%d ",ntruthjet) << truth_jet_pt[ntruthjet]*0.001 << '\t' << Form("truth_jet_eta_%d ",ntruthjet) <<  truth_jet_eta[ntruthjet] << '\t' << Form("truth_jet_phi_%d ",ntruthjet) <<  truth_jet_phi[ntruthjet]  << endl;
		    // }

      }

     outfile <<"truth_n_jets " << *n_truth_jet  <<  endl;

     //electron

     outfile << "============= electron ==============" << endl;

      for(int nel =0; nel<*n_el; nel++)
      {
	//if(fabs(el_eta[nel] > 2.5))
	//{
	    outfile << Form("reco_el_pT_%d ",nel) << el_pt[nel]*0.001 << '\t' << Form("reco_el_eta_%d ",nel) <<  el_eta[nel] << '\t' << Form("reco_el_phi_%d ",nel) <<  el_phi[nel] << endl;

	    // }
		  }

     outfile <<"reco_n_els " << *n_el  <<  endl;

     for(int ntruthel =0; ntruthel<*n_truth_el; ntruthel++)
      {
	//if(fabs(truth_el_eta[ntruthel] > 2.5))
	//{
	outfile << Form("truth_el_pT_%d ",ntruthel) << truth_el_pt[ntruthel]*0.001 << '\t' << Form("truth_el_eta_%d ",ntruthel) <<  truth_el_eta[ntruthel] << '\t' << Form("truth_el_phi_%d ",ntruthel) <<  truth_el_phi[ntruthel] << endl;
	// }

      }

	outfile <<"truth_n_els " << *n_truth_el  <<  endl;

      //mu
      outfile << "============= muons ==============" << endl;
      for(int nmu =0; nmu<*n_mu; nmu++)
      {
	
	outfile << Form("reco_mu_pT_%d ",nmu) << mu_pt[nmu]*0.001 << '\t' << Form("reco_mu_eta_%d ",nmu) <<  mu_eta[nmu] << '\t' << Form("reco_mu_phi_%d ",nmu) <<  mu_phi[nmu] << endl;

      }

     outfile <<"reco_n_mus " << *n_mu  <<  endl;

     for(int ntruthmu =0; ntruthmu<*n_truth_mu; ntruthmu++)
      {
	
	outfile << Form("truth_mu_pT_%d ",ntruthmu) << truth_mu_pt[ntruthmu]*0.001 << '\t' << Form("truth_mu_eta_%d ",ntruthmu) <<  truth_mu_eta[ntruthmu] << '\t' << Form("truth_mu_phi_%d ",ntruthmu) <<  truth_mu_phi[ntruthmu] << endl;

      }

     outfile <<"truth_n_mus " << *n_truth_mu  <<  endl;

      //Boson
      outfile << "============= Bosons:MET:MET nolep phi==============" << endl;
      for(int nv =0; nv<boson_pt.GetSize(); nv++)
      {
	
	outfile << Form("boson_phi_%d ",nv) << boson_phi[nv] << '\t' << Form("met_phi_%d ",nv) <<  *met_phi << '\t' << Form("met_nolep_phi_%d ",nv) <<  *met_tst_nolep_phi << endl;

      }

     outfile << "============= Bosons pt:MET:MET nolep==============" << endl;

     for(int nv =0; nv<boson_pt.GetSize(); nv++)
      {
	
	outfile << Form("boson_pT_%d ",nv) << boson_pt[nv]*0.001 << '\t' << Form("met_%d ",nv) << met << '\t' << Form("met_nolep_%d ",nv) <<  met_nolep << endl;

      }

     outfile << "============= MET from printed smeared var==============" << endl;

     outfile <<"MET " << met_after_cut  <<  endl;

     //outfile <<"truth_n_mus " << *n_truth_mu  <<  endl;

     
     //outfile <<"reco_n_jets " << *n_jet  << '\t' << '\t'<< "reco_n_el " << *n_el  << '\t' << '\t' << "reco_n_mu "  << *n_mu << endl;
  
     /*for(int ntruthel =0; ntruthel<*n_truth_el; ntruthel++)
      {
	
	outfile << Form("truth_pT_%d ",ntruthel) << truth_el_pt[ntruthel]*0.001 << '\t' << Form("truth_eta_%d ",ntruthel) <<  truth_el_eta[ntruthel] << '\t' << Form("truth_phi_%d ",ntruthel) <<  truth_el_phi[ntruthel] << '\t'<< "jetisHS " << jet_isHS[ntruthel] << endl;


      }

     outfile <<"truth_n_jets " << *n_truth_jet  << '\t' << '\t'<< "truth_n_el " << *n_truth_el  << '\t' << '\t' << "truth_n_mu "  << *n_truth_mu << endl;*/
    
    
     //event++;
   return kTRUE;
}

void plotting::SlaveTerminate()
{
   

}

void plotting::Terminate()
{
  //auto c = new TCanvas("c", "c", 800, 800);
   auto c = new TCanvas("c","c");
   c->SetCanvasSize(1500, 1500);
   c->SetWindowSize(500, 500);
   c->SetTicky();
   c->SetTickx();

  /* hist_mjj->Draw();
  c->Print("hist_mjj.pdf");
  
  hist_jj_pT1->Draw();
  c->Print("hist_jj_pT1.pdf");

  hist_jj_pT2->Draw();
  c->Print("hist_jj_pT2.pdf");

  hist_jj_pT3->Draw();
  c->Print("hist_jj_pT3.pdf");

    hist_jj_dphi->Draw();
  c->Print("hist_jj_dphi.pdf");
  
   hist_jj_deta->Draw();
  c->Print("hist_jj_deta.pdf");



  hist_met_soft_tst_et->Draw();
  c->Print("hist_met_soft_tst_et.pdf");

 
  hist_met_significance->Draw();
  c->Print("hist_met_significance.pdf");



  hist_n_jets->Draw();
  c->Print("hist_n_jets.pdf");

  hist_ldJet_eta->Draw();
  c->Print("hist_ldJet_eta.pdf");

  hist_sldJet_eta->Draw();
  c->Print("hist_sldJet_eta.pdf");

  hist_ldJet_phi->Draw();
  c->Print("hist_ldJet_phi.pdf");
  
  hist_sldJet_phi->Draw();
  c->Print("hist_sldJet_phi.pdf");


  hist_j3Cent->Draw();
  c->Print("hist_j3Cent.pdf");

    hist_j3_min_mj_over_mjj->Draw();
  c->Print("hist_j3_min_mj_over_mjj.pdf");


  hist_mj34->Draw();
  c->Print("hist_mj34.pdf");

  hist_maxCentrality->Draw();
  c->Print("hist_maxCentrality.pdf");*/

   TObjArray *MyHistArray = new TObjArray(0);
   MyHistArray->AddLast(hist_pTV);
   MyHistArray->AddLast(hist_pTV_vs_MET_nolep);
   MyHistArray->AddLast(hist_MV);
   MyHistArray->AddLast(hist_dphi_MET_Llep);
   MyHistArray->AddLast(hist_dphi_METnoLep_Llep);
   MyHistArray->AddLast(hist_dphi_MET_PTV);
   MyHistArray->AddLast(hist_dphi_METnoLep_PTV);
   MyHistArray->AddLast(hist_MET_phi);
   MyHistArray->AddLast(hist_METnoLep_phi);
   MyHistArray->AddLast(hist_V_phi);
   MyHistArray->AddLast(hist_el_phi);
   MyHistArray->AddLast(hist_met_tst_et);
   MyHistArray->AddLast(hist_met_tst_nolep_et);

    for (int i = 0; i < MyHistArray->GetEntriesFast(); i++)
	    {
	      /*MyHistArray->At(i)->GetYaxis()->SetTitleOffset(1.35);
	      MyHistArray->At(i)->GetYaxis()->SetLabelSize(0.025);
	      MyHistArray->At(i)->GetXaxis()->SetLabelSize(0.025);*/
	      //MyHistArray->At(i)->Draw();
	    }
   

    const char *histName[] =
    {
      "hist_pTV",
      "hist_pTV_vs_MET_nolep",
      "hist_MV",
      "hist_dphi_MET_Llep",
      "hist_dphi_METnoLep_Llep",
      "hist_dphi_MET_PTV",
      "hist_dphi_METnoLep_PTV",
      "hist_MET_phi",
      "hist_METnoLep_phi",
      "hist_V_phi",
      "hist_el_phi",
      "hist_met_tst_et",
      "hist_met_tst_nolep_et",
         };

    hist_pTV->GetYaxis()->SetTitleOffset(1.35);
    hist_pTV->GetYaxis()->SetLabelSize(0.025);
    hist_pTV->GetXaxis()->SetLabelSize(0.025);

    //histName[0]->
    //gStyle->SetOptStat(0);

  hist_pTV->Scale(1/hist_pTV->Integral());
  hist_pTV->Draw("hist");
  c->Print("hist_pTV.pdf");

    hist_pTV_vs_MET_nolep->GetYaxis()->SetTitleOffset(1.35);
    hist_pTV_vs_MET_nolep->GetYaxis()->SetLabelSize(0.025);
    hist_pTV_vs_MET_nolep->GetXaxis()->SetLabelSize(0.025);
    hist_pTV_vs_MET_nolep->GetZaxis()->SetLabelSize(0.025);
    
  hist_pTV_vs_MET_nolep->Scale(1/hist_pTV_vs_MET_nolep->Integral());
  hist_pTV_vs_MET_nolep->Draw("colz1");
  c->Print("hist_pTV_vs_MET_nolep.pdf");

    hist_MV->GetYaxis()->SetTitleOffset(1.35);
    hist_MV->GetYaxis()->SetLabelSize(0.025);
    hist_MV->GetXaxis()->SetLabelSize(0.025);
    
  hist_MV->Scale(1/hist_MV->Integral());
  hist_MV->Draw("hist");
  c->Print("hist_MV.pdf");

    hist_dphi_MET_Llep->GetYaxis()->SetTitleOffset(1.35);
    hist_dphi_MET_Llep->GetYaxis()->SetLabelSize(0.025);
    hist_dphi_MET_Llep->GetXaxis()->SetLabelSize(0.025);
    
  hist_dphi_MET_Llep->Scale(1/hist_dphi_MET_Llep->Integral());
  hist_dphi_MET_Llep->Draw("hist");
  c->Print("hist_dphi_MET_Llep.pdf");

    hist_dphi_METnoLep_Llep->GetYaxis()->SetTitleOffset(1.35);
    hist_dphi_METnoLep_Llep->GetYaxis()->SetLabelSize(0.025);
    hist_dphi_METnoLep_Llep->GetXaxis()->SetLabelSize(0.025);
    
  hist_dphi_METnoLep_Llep->Scale(1/hist_dphi_METnoLep_Llep->Integral());
   hist_dphi_METnoLep_Llep->Draw("hist");
  c->Print("hist_dphi_METnoLep_Llep.pdf");

  hist_dphi_MET_PTV->GetYaxis()->SetTitleOffset(1.35);
  hist_dphi_MET_PTV->GetYaxis()->SetLabelSize(0.025);
  hist_dphi_MET_PTV->GetXaxis()->SetLabelSize(0.025);
    
  hist_dphi_MET_PTV->Scale(1/hist_dphi_MET_PTV->Integral());
  hist_dphi_MET_PTV->Draw("hist");
  c->Print("hist_dphi_MET_PTV.pdf");

    hist_dphi_METnoLep_PTV->GetYaxis()->SetTitleOffset(1.35);
    hist_dphi_METnoLep_PTV->GetYaxis()->SetLabelSize(0.025);
    hist_dphi_METnoLep_PTV->GetXaxis()->SetLabelSize(0.025);
    
  hist_dphi_METnoLep_PTV->Scale(1/hist_dphi_METnoLep_PTV->Integral());
  hist_dphi_METnoLep_PTV->Draw("hist");
  c->Print("hist_dphi_METnoLep_PTV.pdf");

    hist_MET_phi->GetYaxis()->SetTitleOffset(1.35);
    hist_MET_phi->GetYaxis()->SetLabelSize(0.025);
    hist_MET_phi->GetXaxis()->SetLabelSize(0.025);
    
  hist_MET_phi->Scale(1/hist_MET_phi->Integral());
   hist_MET_phi->Draw("hist");
  c->Print("hist_MET_phi.pdf");

    hist_METnoLep_phi->GetYaxis()->SetTitleOffset(1.35);
    hist_METnoLep_phi->GetYaxis()->SetLabelSize(0.025);
    hist_METnoLep_phi->GetXaxis()->SetLabelSize(0.025);
    
  hist_METnoLep_phi->Scale(1/hist_METnoLep_phi->Integral());
   hist_METnoLep_phi->Draw("hist");
  c->Print("hist_METnoLep_phi.pdf");

    hist_V_phi->GetYaxis()->SetTitleOffset(1.35);
    hist_V_phi->GetYaxis()->SetLabelSize(0.025);
    hist_V_phi->GetXaxis()->SetLabelSize(0.025);
    
  hist_V_phi->Scale(1/hist_V_phi->Integral());
  hist_V_phi->Draw("hist");
  c->Print("hist_V_phi.pdf");


    hist_el_phi->GetYaxis()->SetTitleOffset(1.35);
    hist_el_phi->GetYaxis()->SetLabelSize(0.025);
    hist_el_phi->GetXaxis()->SetLabelSize(0.025);
    
  hist_el_phi->Scale(1/hist_el_phi->Integral());
  hist_el_phi->Draw("hist");
  c->Print("hist_el_phi.pdf");

    hist_met_tst_et->GetYaxis()->SetTitleOffset(1.35);
    hist_met_tst_et->GetYaxis()->SetLabelSize(0.025);
    hist_met_tst_et->GetXaxis()->SetLabelSize(0.025);
    
  hist_met_tst_et->Scale(1/hist_met_tst_et->Integral());
  hist_met_tst_et->Draw("hist");
  c->Print("hist_met_tst_et.pdf");

    hist_met_tst_nolep_et->GetYaxis()->SetTitleOffset(1.35);
    hist_met_tst_nolep_et->GetYaxis()->SetLabelSize(0.025);
    hist_met_tst_nolep_et->GetXaxis()->SetLabelSize(0.025);
    
  hist_met_tst_nolep_et->Scale(1/hist_met_tst_nolep_et->Integral());
  hist_met_tst_nolep_et->Draw("hist");
  c->Print("hist_met_tst_nolep_et.pdf");


   hist_ldJet_eta->GetYaxis()->SetTitleOffset(1.35);
    hist_ldJet_eta->GetYaxis()->SetLabelSize(0.025);
    hist_ldJet_eta->GetXaxis()->SetLabelSize(0.025);
    
  hist_ldJet_eta->Scale(1/hist_ldJet_eta->Integral());
  hist_ldJet_eta->Draw("hist");
  c->Print("hist_ldJet_eta.pdf");


     hist_j3EtaPt->GetYaxis()->SetTitleOffset(1.35);
    hist_j3EtaPt->GetYaxis()->SetLabelSize(0.025);
    hist_j3EtaPt->GetXaxis()->SetLabelSize(0.025);
    
  hist_j3EtaPt->Scale(1/hist_j3EtaPt->Integral());
  hist_j3EtaPt->Draw("colz");
  c->Print("hist_j3EtaPt.pdf");




  
  
  if (fOut && fOut->IsOpen()) {
    fOut->Write();
    fOut->Close();
  } 

}
